import React, {Component} from 'react';
import { StyleSheet, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container,
          Body, Header, Content,
          Form, Item, Input, Label, Title, Button, Text} from 'native-base';
import firebase from 'firebase';

export default class RegisterForm extends Component {

  constructor(props){
        super(props)
        this.state = ({
          email: '',
          password: '',
          loading: false,
            error: ''

        })
    }

    onClickSignUp(){
          this.setState({error: '', loading:true})
          const {email, password} = this.state;

          firebase.auth().createUserWithEmailAndPassword(email, password)
          .then(() => {
              this.setState({error:'', loading: false});
              Actions.login();

          })
          .catch(() =>{

          //form validation
            if(this.state.email.length < 6) {
               alert('emails are usually longer than this')
                 this.setState({error: 'Emails are usually longer', loading:false});
               return;
            }

            if(this.state.password.length < 8) {
               alert('Password must be greater than 8 characters')
                 this.setState({error: 'Password must be greater than 8 characters', loading:false});
               return;
            }

          })
    }

  render() {
    return (
      <Container>
        <Content>
          <Form>
            <Item floatingLabel>
              <Label>
                Email
              </Label>
              <Input
                 onChangeText={email => this.setState({email})}
              >
              </Input>
            </Item>
            <Item floatingLabel>
              <Label>
                Password
              </Label>
              <Input
               onChangeText={password => this.setState({password})}
               secureTextEntry
              >
              </Input>
            </Item>
            <Button
               onPress={this.onClickSignUp.bind(this)}
            primary block>
              <Text>Register</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}

import React, { Component } from 'react';
import { ListView, StatusBar } from 'react-native';
import { Container, Content, Button, Icon, List, ListItem, Input, Label, Form, Text, Header, Item} from 'native-base';
import {connect} from 'react-redux';
import {getStudioList} from '../actions/index';
import firebase from 'firebase';


const datas = [
  'Redspark',
  'Nathaniel Clyne',
  'Dejan Lovren',
  'Mama Sakho',
  'Alberto Moreno',
  'Emre Can',
  'Joe Allen',
  'Phil Coutinho',
];

class Studios extends Component {
  constructor(props) {
    super(props);
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      basic: true,
      listViewData: datas,
      newContact: ""
    };
  }

  addRow(datas){
    var key = firebase.database().ref('/studio').push().key
    firebase.database().ref('/studio').child(key).set({name: datas})
  }

//  componentDidMount(){
  //  var that = this;

    //firebase.database().ref('/studio').on('child_added', function(data){

    //  var newData = [...that.state.listViewDalkta]
    //  newData.push(data)
      //that.setState({listViewData: newData})
    //})

  //}


  componentWillMount(){

    this.props.getStudioList();
  }

   deleteRow(secId, rowId, rowMap) {

    rowMap[`${secId}${rowId}`].props.closeRow();
    const newData = [...this.state.listViewData];
    newData.splice(rowId, 1);
    this.setState({ listViewData: newData });
  }
  render() {
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    return (
      <Container>
        <Header />
        <Header style={{marginTop: StatusBar.currentHeight}}>
          <Content>
            <Item>
              <Input
                onChangeText={(newContact) => this.setState({newContact})}
                placeholder="add New"
              />
              <Button onPress={()=>this.addRow(this.state.newContact)}>
                <Icon name="add" />
              </Button>
            </Item>
          </Content>
        </Header>
        <Content>
          <List
          enableEmptySections
            dataSource={this.ds.cloneWithRows(this.props.favourite_list)}
            renderRow={data =>
              <ListItem>
                <Text> {data.name} </Text>
              </ListItem>}
            renderLeftHiddenRow={data =>
              <Button full onPress={() => alert(data)}>
                <Icon active name="information-circle" />
              </Button>}
            renderRightHiddenRow={(data, secId, rowId, rowMap) =>
              <Button full danger onPress={_ => this.deleteRow(secId, rowId, rowMap)}>
                <Icon active name="trash" />
              </Button>}
            leftOpenValue={75}
            rightOpenValue={-75}
          />
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = ({ studio }) => {
  const { favourite_list, loading } = studio;

  return {favourite_list, loading}
}

export default connect(mapStateToProps, {getStudioList})(Studios);

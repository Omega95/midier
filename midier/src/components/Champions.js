import React, { Component } from 'react';
import { ListView, StatusBar } from 'react-native';
import { Container, Content, Button, Icon, List, ListItem, Input, Label, Form, Text, Header, Item} from 'native-base';
import {
  fetchChampions
} from '../actions/index';


// const datas = [
//   'Simon Mignolet',
//   'Nathaniel Clyne',
//   'Dejan Lovren',
//   'Mama Sakho',
//   'Alberto Moreno',
//   'Emre Can',
//   'Joe Allen',
//   'Phil Coutinho',
// ];

const datas = [
  {
    name: 'Simon Mignolet',
    favourite: true
  },
  {
    name: 'Nathaniel Clyne',
    favourite: false
  }
];

class Champions extends Component {

  constructor(props) {
    super(props);
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      basic: true,
      listViewData: datas,
    };
  }



  addRow(data){
    var key = firebaseApp.database().ref('/studio').push().apiKey
    firebaseApp.database().ref('/studio').child(key).set({name: data})
  }

  deleteRow(){

  }

  showInformation(){

  }

  modifyRow(secId, rowId, rowMap) {
    rowMap[`${secId}${rowId}`].props.closeRow();
    const newData = [...this.state.listViewData];
    newData.splice(rowId, 1);
    this.setState({
      listViewData: newData,
      newContact: ""
    });
  }

  renderRight(data, secId, rowId, rowMap) {
    return (
      <Button full danger onPress={_ => this.modifyRow(secId, rowId, rowMap)}>
        <Icon active name="add" />
      </Button>
    );
  }

  render() {
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

    return (
      <Container>
            <Header style={{marginTop: StatusBar.currentHeight}}>
              <Content>
                <Item>
                  <Input
                    onChangeText={(newContact) => this.setState({newContact})}
                    placeholder="add New"
                  />
                  <Button onPress={()=>this.addRow(this.state.newContact)}>
                    <Icon name="add" />
                  </Button>
                </Item>
              </Content>
            </Header>

        <Content>
          <List
            enableEmptySections
            dataSource={this.ds.cloneWithRows(this.state.listViewData)}
            renderRow={data =>
              <ListItem>
                <Text> {data.name} </Text>
              </ListItem>
            }

            renderLeftHiddenRow={data =>
              <Button full onPress={() => this.addRow(data)}>
                <Icon name="information-circle" />
              </Button>
            }

            renderRightHiddenRow={(data, secId, rowId, rowMap) =>
              <Button full onPress={() => this.deleteRow(secId, rowId, rowMap, data)}>
                <Icon name="trash" />
              </Button>
            }

            leftOpenValue={-75}
            rightOpenValue={-75}

          />
          {this.props.email}
        </Content>
      </Container>
    );
  }
}

export default Champions;
// export default connect(null, { fetchChampions })(Champions);


/*
componentWillMount() {
    fetchChampions().then(data => {
      console.log(data);
    });
  }
*/

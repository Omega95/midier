import React from 'react';
import {StyleSheet, View, Image} from 'react-native';

var logoIMG = require('../../assets/LoginAssets/bridgeLayersAlternateG.png');

class Logo extends React.Component{
  render(){
    return(
      <View style={styles.logoStyle}>
        <Image source={logoIMG}/>
      </View>
    )
  }
}

let styles = StyleSheet.create({
  logoStyle: {
      marginLeft:60,
      marginTop: 120,
  },
});

export default Logo;

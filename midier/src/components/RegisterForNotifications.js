import { Permissions,  Notifications } from 'expo';


export default (async function RegisterForNotifications() {
  let { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);

  if(status !== 'granted'){
    return;
  }

  let token = await Notifications.getExpoPushTokenAsync();

  userID = firebaseApp.auth().currentUser.uid;

  console.log(token)

  firebaseApp.database().ref('/users/' + userID).set({ token: token });

}

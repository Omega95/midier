import React, { Component } from 'react';
import { StyleSheet, View, Image, Platform} from 'react-native';
import { Actions } from "react-native-router-flux";
import { Container,
          Body, Header, Content,
          Form, Item, Input, Label, Title, Button, Text, H1} from 'native-base';
import { connect } from 'react-redux';
import { emailChange, passwordChange, loginUser } from '../actions';
import Logo from './Logo';
import styled from 'styled-components';
import {CommonContainer, LoadingButton, CommonField, CommonPassword} from './Common'

const TermsButtonStyle = styled(Text)`
   font-size:14px;
   text-align:center;
   color:#424242;
   margin-top:40px;
 `;

class Login extends Component {

  onEmailChange( email ) {
    this.props.emailChange( email );
  }

  onPasswordChange( password ) {
    this.props.passwordChange( password );
  }

  onLoginBtnPress() {
    const {email, password} = this.props;
    this.props.loginUser({email, password});
  }

    async logInFB() {
      const { type, token } = await Expo.Facebook.logInWithReadPermissionsAsync
      ('443259246109111', { permissions: ['public_profile'] });
      if (type === 'success') {

        const credential = firebaseApp.auth.FacebookAuthProvider.credential(token)

        firebaseApp.auth().signInWithCredential(credential).catch((error) => {
          console.log(error)
        })
      }
  }

  render() {
    return (
      <CommonContainer>
        <Logo />
          <Content>
            <CommonField
              value={this.props.email}
              onChangeText={this.onEmailChange.bind(this)}
              style={{marginTop: 30, marginLeft: -10}}
              label="Email"
            />
            <CommonPassword
              value={this.props.password}
              onChangeText={this.onPasswordChange.bind(this)}
              style={{marginTop: 30, marginLeft: -10}}
              label="Password"
            />
            <LoadingButton
              loading={this.props.loading}
              style={styles.buttonStyle}
              onPress={(this.onLoginBtnPress.bind(this))}
              text="Login"
            />
            <LoadingButton
              loading={this.props.loading}
              style={styles.facebookButton}
              onPress={this.logInFB.bind(this)}
              text="Login with Facebook"
            />
            <TermsButtonStyle onPress={() => Actions.forgotPassword()}>Forgot Password?</TermsButtonStyle>
            <TermsButtonStyle onPress={() => Actions.register()}>Create A New Account</TermsButtonStyle>
          </Content>
      </CommonContainer>
    );
  }
}

let styles = StyleSheet.create({
  backgroundImage: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    resizeMode: 'stretch',
  },

  preloader :{
    width: 380,
    height: 900,
    backgroundColor:'#fff',
    position: 'absolute',
    alignItems: 'center',
    paddingTop: 420,
    zIndex: 2,
    elevation: 3
  },

  buttonStyle: {
    marginTop: 35,
  },

  facebookButton: {
    backgroundColor: '#4d6cb7',
    marginTop: 35
  }

});

const mapStateToProps = ({auth}) => {
  const {email, password } = auth;

  return { email, password };
};

export default connect(mapStateToProps, { emailChange, passwordChange, loginUser })(Login);

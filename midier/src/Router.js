import React, {Component} from "react"
import { Router, Scene, Stack, Tabs } from "react-native-router-flux"
import { Icon } from 'native-base'

import Login from "./components/Login"
import RegisterForm from "./components/RegisterForm"
import ForgotPassword from "./components/ForgotPassword"
import Profile from './components/Profile'
import Champions from './components/Champions';
import Studios from './components/Studios';


class RouterComponent extends Component{
    render() {
       return(
        <Router>
            <Scene key="root">
                <Scene
                    key="login"
                    component={Login}
                    hideNavBar={true}

                />
                <Scene
                    key="register"
                    component={RegisterForm}
                    hideNavBar={false}
                    title="Register"
                />
                <Scene
                    key="forgotPassword"
                    component={ForgotPassword}
                    hideNavBar={false}
                    title="Forgot Password"
                />

                <Scene
                    key="main"
                    hideNavBar
                    panHandlers={null}
                >
                    <Tabs key="tabbar">
                        <Scene
                            key="profile"
                            title="Studios"
                            component={Studios}
                            icon={() => <Icon name="person" />}
                        />
                        <Scene
                            key="champions"
                            title="Champions"
                            component={Champions}
                            icon={() => <Icon name="people" />}
                        />
                    </Tabs>
                </Scene>

            </Scene>
        </Router>
       );
    }
}

export default RouterComponent;

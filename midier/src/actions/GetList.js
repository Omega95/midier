import firebase from 'firebase';
import {GET_FAVOURITES, ADD_FAVOURITES, FETCH_DONE} from './types';

export const getStudioList = () => {

  const db = firebase.firestore();


      return (dispatch) => {

        dispatch({type: GET_FAVOURITES});


        db.collection("studio").get().then(function(querySnapshot) {
          dispatch({type: FETCH_DONE})
        querySnapshot.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
            console.log(doc.id, " => ", doc.data());

            dispatch({
              type: ADD_FAVOURITES,
              payload: doc.data()
            });
        });
    });
  }
}

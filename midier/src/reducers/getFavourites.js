import {GET_FAVOURITES, ADD_FAVOURITES, FETCH_DONE} from '../actions/types';

const favourite = {
  favourite_list: [],
  loading: true
}

export default(state = favourite, action) => {

  switch(action.type){
    case GET_FAVOURITES:

      return{...state, favourite_list: [], loading: true};

    case ADD_FAVOURITES:


      return{...state, favourite_list: [...state.favourite_list, action.payload]};

    case FETCH_DONE:

      return{...state, loading: false};

    default:
        return state;
  }

}

import { combineReducers } from 'redux';
import AuthReducer from "./AuthReducer";
import getFavourites from "./getFavourites";

export default combineReducers({
    auth: AuthReducer,
    studio: getFavourites
});

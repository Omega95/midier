import React, { Component } from 'react';
import Router from "./src/Router";

import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import ReduxThunk from "redux-thunk";
import reducers from "./src/reducers";
import { Root } from "native-base";
import firebase from 'firebase';

/**
* Firebase quirk for firestore
*/
require('firebase/firestore');

export default class App extends Component {
  componentWillMount() {

      var config = {
        apiKey: "AIzaSyDU_d6ZN5weaZY5HSudwOK2FbK3YExTxik",
         authDomain: "favourites-d6bcd.firebaseapp.com",
         databaseURL: "https://favourites-d6bcd.firebaseio.com",
         projectId: "favourites-d6bcd",
         storageBucket: "favourites-d6bcd.appspot.com",
         messagingSenderId: "120403167847"
      };
      firebase.initializeApp(config);

  }

  registerForPushNotificationsAsync = async () =>{

    const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
      );
      let finalStatus = existingStatus;

      // only ask if permissions have not already been determined, because
      // iOS won't necessarily prompt the user a second time.
      if (existingStatus !== 'granted') {
        // Android remote notification permissions are granted during the app
        // install, so this will only ask on iOS
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        finalStatus = status;
      }

      // Stop here if the user did not grant permissions
      if (finalStatus !== 'granted') {
        return;
      }

      // Get the token that uniquely identifies this device
      let token = await Notifications.getExpoPushTokenAsync();

  }

  render() {

    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk) );

    return (
      <Provider
      store={store}
      >
      <Root>
        <Router/>
      </Root>
      </Provider>
    );
  }
}
